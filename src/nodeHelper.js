'use strict';

module.exports.set_node_status_properties = function (statusValue, statusText) {

    var fillValue = "red";
    var shapeValue = "dot";

    switch (statusValue) {
        case "configError":
            fillValue = "red";
            shapeValue = "ring";
            break;
        case "config":
            fillValue = "green";
            shapeValue = "ring";
        break;
        case "warning":
            fillValue = "yellow";
            shapeValue = "ring";
        break;
        case "msg":
            fillValue = "green";
            shapeValue = "ring";
            break;
        default:
            if (!statusValue || statusValue == "waiting") {
                fillValue = "blue";
                statusValue = "waiting ...";
            }
            break;
    }

    return {fill: fillValue, shape: shapeValue, status: statusText};
};