/**
 Copyright (c) 2016,2017, Klaus Landsdorf (http://bianco-royal.de/)
 All rights reserved.
 node-red-contrib-modbus - The BSD 3-Clause License

 @author <a href="mailto:klaus.landsdorf@bianco-royal.de">Klaus Landsdorf</a> (Bianco Royal)
 **/
'use strict'
let fileName = null;
let isLoggerConfigured = false;
let log = null;
const fs = require('fs');
let logData = require("./logging")

let loggingVal 
let filenameVal 
let filepathVal 
let filesizeVal 
let sizeUnitVal 
let logLevelVal

module.exports.configure = function (logging, filename , filepath, filesize, sizeUnit, logLevel){

      let logDir = null;
      
      let fileSize = 0;

       loggingVal = logging
       filenameVal = filename
       filepathVal = filepath
       filesizeVal = filesize
       sizeUnitVal = sizeUnit
       logLevelVal = logLevel
      
      const winston = require('winston');
      if(!logging || isLoggerConfigured)
        return;

      if(sizeUnit == 'kb')
          fileSize = filesize * 1024
      else if(sizeUnit == 'mb')
          fileSize = filesize * 1048576
      else if(sizeUnit == 'gb')
          fileSize = filesize * 1073741824
      else
        fileSize = 1048576



      if(filepath == "")
          logDir = __dirname +'/log';
      else
          logDir = filepath +'/log'

      fileName =logDir + '/' +  filename +'.log'

    // Create the log directory if it does not exist
    if (!fs.existsSync(logDir)) {
      fs.mkdirSync(logDir);
    }

      const tsFormat = () => (new Date()).toLocaleTimeString();
      log = new (winston.Logger)({
      transports: [
        new (winston.transports.File)({
          filename: fileName,
          timestamp: tsFormat,
          maxsize:fileSize,
          maxFiles:2,
          tailable: true,
          level: 'debug'
        })
      ]
    });

    isLoggerConfigured = true;
}
//const log = logger.getLogger("Modbus");
module.exports.logInfo = function (logMessage , isLoggingEnabled,logLevel){
  if(isLoggingEnabled && logLevel != 'error'){
      log.info(logMessage);
      //checkFileExistsSync(fileName)
  }
}

module.exports.logError = function (logMessage, isLoggingEnabled,logLevel){
  if(isLoggingEnabled){
      log.error(logMessage);
      //checkFileExistsSync(fileName)
  }
}

module.exports.debugMessage = function (logMessage, isLoggingEnabled, logLevel){
  if(isLoggingEnabled && logLevel != 'error' && logLevel != 'info'){
      log.debug(logMessage)
      //checkFileExistsSync(fileName)
  }
}

  function checkFileExistsSync(filepath){
    if (!fs.existsSync(filepath)) {
      isLoggerConfigured = false;
      //log.close();
      //logData.configure(loggingVal, filenameVal ,filepathVal , filesizeVal , sizeUnitVal , logLevelVal);
    }
  }