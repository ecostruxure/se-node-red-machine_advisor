//==========================================================================================================================
// Node            : SE_Machine_Advisor
// Description     : Node to upload data to Machine Advisor.                   
// Configuration   : Configuration Properties to be set to the Node:
//                      1) Name of the Node
//                      2) Register Type (Function Code)
//                      3) Start Register Address
//                      4) Quantity (Number of registers)
//                      5) Retry Limit
//                      6) Retry Frequency
//                      7) Modbus Slave details (slave address, COM Port Details)
// Inputs          : Inputs to the Node:
//                      1) data to be sent to Machine Advisor
// Outputs         : Outputs from the Node:
//                      1) Http Status Code
// Created by      : Jorge-Adrian Alvarez
// Creation Date   : 01-06-2017
// Modified by     : RamónA. Hormigo
// Modified Date   : 12-07-2018
// git Repository  : 
// License         : Licensing/Copyright terms provided by Scheider Electric Node-Red Community
//==========================================================================================================================

module.exports = function (RED) {

    // Execute this code in Strict Mode
    "use strict";

    // Get all the required Modules
    var helper = require("../src/nodeHelper");
    var request = require("request");
    let logData = require("../src/logging")

    //===============================================================================
    // Node Constructor
    //===============================================================================
    function SEMachineAdvisor(config) {

        // Initialize the Node
        RED.nodes.createNode(this, config);

        var node = this;

        var interval = null;
        var configError = null;
        var requesttimeOut = 0;

        // get the configuration properties to local variables
        this.name = config.name;

        this.logging = config.logging
        this.loggingLevel = config.loggingLevel
        this.size = 5
        this.sizeUnit = "mb"

        if(node.logging === "1")
            node.logging = true;
        else
           node.logging = false;

        logData.configure(node.logging ,"Machine Advisor" ,  __dirname, node.size, node.sizeUnit, node.loggingLevel)

        set_node_status_to("config","Configured");

        if (config.server == "") {
            set_node_status_to("configError", "ConfigError");
            logData.logError("NODE Name::"+ node.name + ":" + "NODE ID::"+ node.id + "::Server field is not configured",node.logging,node.loggingLevel);
            configError = true;
        }
        else {
            this.server = config.server;
        }
        
        if (config.customheader2 == "") {
            set_node_status_to("configError", "ConfigError");
            logData.logError("NODE Name::"+ node.name + ":" + "NODE ID::"+ node.id +"::Custom Header 2 field is not configured",node.logging,node.loggingLevel);
            configError = true;
        }
        else {
            if (config.customheader2.startsWith("Authorization;")) {
				this.customheader2 = config.customheader2.replace("Authorization;","");
			} else {
				this.customheader2 = config.customheader2;
			}
        }
        

        if (config.useProxy == "yes") {

            if (config.proxy == "") {
                set_node_status_to("configError", "ConfigError");
                logData.logError("NODE Name::"+ node.name + ":" + "NODE ID::"+ node.id +"::Proxy field is not configured",node.logging,node.loggingLevel);
                configError = true;
            }
            else {
                this.useProxy = config.useProxy;
                this.proxy = config.proxy;
            }
        }

        this.timeout = config.timeout;
        if(config.timeout > 5)
            requesttimeOut = node.timeout * 1000
         else
            requesttimeOut = 5000

        // input event Handler
        this.on("input", function (msg) {
            
            if(!configError){
                var options;
                if (config.useProxy == "yes" && node.proxy != "") {
                    options = {
                        method: 'POST',
                        url: node.server,
                        proxy: node.proxy,
                        timeout: requesttimeOut,
                        headers:
                        {
                            "content-type": "application/json",
                            "authorization": node.customheader2
                        },
                        body:msg.payload,
                        json: true
                    };
                }
                else {
                    options = {
                        method: 'POST',
                        url: node.server,
                        timeout: requesttimeOut,
                        headers:
                        {
                            "content-type": "application/json",
                            "authorization": node.customheader2
                        },
                        body: msg.payload,
                        json: true
                    };
                }
                set_node_status_to("msg","Waiting for reponse");
                
               

                request(options, function (error, response, body) {
                    if (error) {
                        clearTimeout(interval);
                        interval = setTimeout(update_node_status, 5000); 
                        set_node_status_to("configError","Error");
                        msg.payload = { "Error": "Data is not pushed. " + error };
                        logData.logError("NODE Name::"+ node.name + ":" + "NODE ID::"+ node.id +"::Data is not pushed",node.logging,node.loggingLevel);
                        node.send(msg);
                    }
                    else {
                        clearTimeout(interval);
                        interval = setTimeout(update_node_status, 5000); 
                        msg.payload = { "Response": response };
                        msg.options = { "options": options };
                        if(response.statusCode  == 204){
                            set_node_status_to("msg","Success");
                            logData.logInfo("NODE Name::"+ node.name + ":" + "NODE ID::"+ node.id +"::Data is pushed successfully",node.logging,node.loggingLevel);                            
                        }
                        else{
                            msg.payload = { "Error": "Data is not pushed" };
                            logData.logError("NODE Name::"+ node.name + ":" + "NODE ID::"+ node.id +"::Data is not pushed",node.logging,node.loggingLevel);
                            set_node_status_to("configError","Error");
                        }

                        node.send(msg);
                
                    }
                });
        }
        else{
            
            set_node_status_to("configError","ConfigError");
            check_configuration_error()
        }

         // Error Message.
         function check_configuration_error() {
            if (config.server == "") {
                msg.payload =   {"Error":"Server field is not configured" };
                node.send(msg);
            }
            
            if (config.customheader2 == "") {
                msg.payload =   {"Error":"Custom Header 2 field is not configured" };
                node.send(msg);
            }
            
            if (config.useProxy == "yes") {
    
                if (config.proxy == "") {
                    msg.payload =   {"Error":"Proxy field is not configured" };
                    node.send(msg);
                }
            }
        }

            return;
        });
        // Helper Function
        function set_node_status_to(statusValue, statusText) {
            var statusOptions = helper.set_node_status_properties(statusValue, statusText);

            node.status({
                fill: statusOptions.fill,
                shape: statusOptions.shape,
                text: statusOptions.status
            });
        }

        // Configure the node status if no input is
        function update_node_status() {
            set_node_status_to("config","Configured");
            clearTimeout(interval);
        }
    }

    // Register this Node with node-red Runtime, it appears in floweditor
    RED.nodes.registerType("SE_Machine_Advisor", SEMachineAdvisor);
}
