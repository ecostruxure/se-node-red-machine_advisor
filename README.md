 # se-node-red-machine_advisor [![Build Status]()]

 > Machine Advisor nodes to upload data to Machine Advisor cloud.

 ## Description

 This Node Package consists of 1 Node-Red Node:
  1. SEMachineAdvisor
 

 #### SEMachineAdvisor

 Connects to a Machine Advisor Cloud and pushes the data when the input is received.
 
 Below are the necessary configurations required.
  * Server: Text field to provide the server link of Machine Advisor where data will be pushed. 
  * Header 2: Header 2 of Machine Advisor.
  * Use Proxy:Proxy in case of network restriction. Its default value is "No". 
  * Timeout:integer containing the number of seconds or minutes to wait for a server to send response 
    headers (and start the response  body) before aborting the request. Note that if the underlying TCP connection cannot be established, the OS-wide TCP connection timeout will overrule the Timeout option.
  * Logging: Configuration parameter to Enable or Disable the logging. Logging is by default Enabled.
  * Level : Configuration parameter for selecting the logging level.There are three logging levels- Info, Debug and Error. Its default value is Error.

 Below is input/output details for SEMachineAdvisor node.

### Input 
  Json Data: As per the Machine Advisor specifications.
```sh
      Format Tango
      Tango is a json format that Machine Advisor supports
      {
        "ID":"prefix string", 
        "TS":"Timestamp string", 
        "ST":"Start Adress number", 
        "VR":"Array of numbers"
      }
      Example:
      {
        "ID":"SampleID", 
        "TS":"18/01/2018 07:13:00", 
        "ST":"200", 
        "VR":[123.4,5000,-100]
      }

      Format Charlie
      Charlie is a json format that Machine Advisor supports
      {
        "metrics": {
          "assetName": "prefix string",
          "Variable1": "number",
          "Variable1_timestamp": "Epoc Time for Variable1",
          "Variable2": "number",
          "Variable2_timestamp": "Epoc Time for Variable2"
        }
      }
      Example:
      {
        "metrics": {
          "assetName": "M241",
          "Variable1": 99,
          "Variable1_timestamp": 1524579710733,
          "Variable2": 88,
          "Variable2_timestamp": 1524579710733
       }
      }
```    
 ### Output 
  * Cloud response status code in case of successful data push. 
  * Failure message if data push is not  successful. 
	
 ## Prerequisite   
  * A 64 bit machine, Windows or Linux.
  * Node.js version 6.11.0 and above.
  * Node-RED server.
  * Any latest web browser with V8 engine(Advisabe chrome and firefox).
  * Wonderware online Account.
  * Wonderware data tag in wonderware cloud.  
   
 ## Installation

 Use the below command to install Node-RED server. Restart the Node-RED server after installing the node.

 ### Windows
  1. Install Node.js software version 6.11.0 and above. It can be downloaded from below link.
  </br><a href="https://nodejs.org/en/" target="_blank"> https://nodejs.org/en/</a> 
  2. Install Node-RED. Run "npm install -g node-red" command on command prompt.  

 ### Linux
  1. Install Node.js software. Follow the procedure mentioned in the below link.  
  <a href="https://nodejs.org/en/download/package-manager/" target="_blank"> https://nodejs.org/en/download/package-manager/</a> 
  2. Install Node-RED.Run "sudo npm install -g --unsafe-perm node-red" command on Linux terminal.  

 Use the below command to install se-node-red-wonderware_online package (make sure you set the npm registry to internal schneider electric registry)

 ```sh
  $ npm install git+https://git@bitbucket.org/ecostruxure/se-node-red-machine_advisor.git
 ```
 ## Uninstallation

 Use the below command to uninstall se-node-red-wonderware_online package for Linux and windows platform.
  ```sh
  $ npm uninstall se-node-red-machine_advisor
 ```
 If SEMachineAdvisor is present in node flow follow these steps to uninstall
  * Delete the nodes from the flow. 
  * Run the uninstall command (npm uninstall se-node-red-machine_advisor).
  * After uninstallation restart the Node-RED server.

 ## Usage
 #### SE Wonderware Online Send
  1. Copy the server address of Machine Advisor Monitor Configuration where data to be pushed.
  2. Copy the header 2 from Machine Advisor Monitor Configuration machine.
  3. Configure Proxy in case of network restriction. Its default value
     is no.
  4. Inject input data to node.

  ## Dependencies

 This module depends on the following modules:
  * jsonfile
  * request
  * winston

## Known Issues
 None

## Limitation
  Timeout - integer containing the number of milliseconds to wait for a server to send response headers (and start the response body) 
  before aborting the request. Note that if the underlying TCP connection cannot be established, the OS-wide TCP connection timeout will 
  overrule the timeout option.For example timeout overrule may happen when user configures the proxy field of node and network has no proxy.

## Additional Information
 * User must have Machine Advisor Account.
 * User must have created new Machine and access to Monitor Configuration .
 * Log file is saved inside the log folder.Log folder is creted if logging is enabled inside node folder of existing installed  
    se-node-red-machine_advisor.Log file is saved with name MachineAdvisor.log and MachineAdvisor1.log(Backup).
 *  The log file size is fixed to 5MB. Once the 5MB limit is crossed a new log file is created and the previous file is preserved as     
     backup. If this new file again reaches 5MB the existing backup file is first deleted and then the current file is preserved as      
     backup and a new file is created.This procedure is always repeated and the total space consumed would never cross 10MB.
    Eg: Wonderware.log-created
    - file1- reaches 5mb- file2 - created -File1 is backup file
    - file2 reaches 5mb - File1 is deleted - File2 is made backup file and file3 is created.
 * If seleted Level in logging is error only error messages are logged.If selected level in logging is info both error and info messages are logged.
   If selected level in logging is debug all log messages error , info and debug are logged.   
 * File size is 5MB for all the se-node-red-machine_advisor in the flow. 
   Logs can be diffrentiated by node ID in the Log file.
 * User should not delete the log file if node red server is running, It will stop the data logging. 
   If by mistake user delete the current log file, user need to restart the server to start file logging again. 
 * Time stamp for any node in the log file may differ from the time stamp in debug window.  
 * Validation for minimum and maximum node name length is not implemented.       

## References
<a href="https://machine-advisor.schneider-electric.com/" target="_blank"> https://machine-advisor.schneider-electric.com/</a></br> 


## Contributers
 * Ramón Antonio Hormigo Luque (ramon-antonio.hormigo@schneider-electric.com)
 * Fernando Sanchez (fernando.miravalles@schneider-electric.com)


## History
 * 1.0.0 : A Node-RED Package for Machine Advisor cloud.
